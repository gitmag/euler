/** Problem Three - Largest prime factor of 600851475143. Answer = 6857 */
import java.util.*;

class ProblemThree {
	public static void main(String[] args) {
		System.out.println("Hello");

		ArrayList<Long> factors = new ArrayList<Long>();
		long number = 600851475143L;
		long i = 3L;
		long j = number;

		while (i < j) {
			if (number % i == 0L) {
				factors.add(i);
			}
			j = number / i;
			i = i + 2L;
		}
		System.out.println(Long.toString(i));
		for (Long n: factors) {
			System.out.println(Long.toString(n));
		}
	}
}
