/* Find the sum of all primes below two million */
package euler;

class ProblemTen {
    public static void main(String[] args) {
        NumberOperator asdf = new NumberOperator();
        // Starting at 2 because then the loop can skip 1000000 even integers easily
        long sumOfPrimes = 2L;
        for (long i = 1;i < 2000000;i += 2) {
            if (asdf.isPrime(i)) {
                sumOfPrimes += i;
            }
        }
        System.out.println("The sum of the primes below 2000000 is " +
            Long.toString(sumOfPrimes));
    }
}
