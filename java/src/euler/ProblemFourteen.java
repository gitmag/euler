/*The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:
13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

It can be seen that this sequence (starting at 13 and finishing at 1)
contains 10 terms. Although it has not been proved yet (Collatz Problem),
it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?
*/
public class ProblemFourteen {
    public static void main(String[] args) {
        int currentNumber;
        int currentLength = 2; // 2 to include starting number and final 1
        int longest = 0;
        int numberProducingLongest = 0;
        for (int i = 999999; i > 900000; i--) {
            currentNumber = i;
            while (currentNumber != 1) {
                currentNumber = checkEvenOrOddAndThings(currentNumber);
                currentLength++;
            }
            if (currentLength > longest) {
                numberProducingLongest = i;
            }
            currentLength = 1;
        }
        System.out.println(Integer.toString(numberProducingLongest));
    }

    public static int checkEvenOrOddAndThings(int n) {
        if (n % 2 == 0) {
            return (n / 2);
        } else {
            return (3 * n + 1);
        }
    }
}
