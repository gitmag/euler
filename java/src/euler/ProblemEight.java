/** Find thirteen adjacent digits in an obnoxiously long number with the greatest product */
import java.util.*;

class ProblemEight {
    public static void main(String[] args) {
        String number =  "731671765313306249192251196744265747423553491949349698" +
        "35203127745063262395783180169848018694788518438586156078911294949545950" +
        "173795833195285320880551112540698747158523863050715693290963295227443043" +
        "5576689664895044524452316173185640309871112172238311362229893423380308135" +
        "3362766142828064444866452387493035890729629049156044077239071381051585930" +
        "796086670172427121883998797908792274921901699720888093776657273330010533" +
        "6788122023542180975125454059475224352584907711670556013604839586446706324" +
        "4157221553975369781797784617406495514929086256932197846862248283972241375" +
        "6570560574902614079729686524145351004748216637048440319989000889524345065" +
        "8541227588666881164271714799244429282308634656748139191231628245861786645" +
        "83591245665294765456828489128831426076900422421902267105562632111110937054" +
        "421750694165896040807198403850962455444362981230987879927244284909188845801" +
        "561660979191338754992005240636899125607176060588611646710940507754100225698" +
        "315520005593572972571636269561882670428252483600823257530420752963450";

        MakeBacon asdf = new MakeBacon();
        ArrayList<Integer> numberArray = asdf.stringToArray(number);
        long biggestNumber = 0L;
        long currentNumber = 1L;
        int count = 0;
        for (int i = 0;i < numberArray.size() - 12;i++) {
            if (numberArray.get(i) == 0) { continue; }
            for (int j = i; j < i + 13; j++) {
                currentNumber = currentNumber * (long)numberArray.get(j);
            }
            if (currentNumber > biggestNumber) {
                biggestNumber = currentNumber;
                System.out.println("Current biggest number is " + Long.toString(biggestNumber));
            }
            currentNumber = 1L;
        }
        System.out.println("The largest product of 13 consecutive digits is " + Long.toString(biggestNumber));
    }
}

class MakeBacon {

    public ArrayList<Integer> stringToArray(String s) {
        ArrayList<Integer> numberArray = new ArrayList<Integer>();

        for (int i = 0;i < s.length();i++) {
            numberArray.add(Integer.parseInt(s.substring(i, i+1)));
        }

        return numberArray;
    }
}
