/* Largest palindrome made from the product of 2 3-digit numbers
 * ie largest from two 2 digit numbers - 9009 = 91 * 99
 * Correct answer = 906609
 */
 import java.util.*;

class ProblemFour {
    public static void main(String[] args) {
        ArrayList<Integer> demPal = new ArrayList<Integer>();

        for (int i = 999; i > 100; i--) {
            for (int j = 999; j > 100; j--) {
                if (Integer.toString(i * j).equals(new StringBuilder(Integer.toString(i * j)).reverse().toString())) {
                    demPal.add(i*j);
                }
            }
        }

        System.out.println(Integer.toString(Collections.max(demPal)));
    }
}
