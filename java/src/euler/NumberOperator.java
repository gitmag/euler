package euler;

public class NumberOperator {
    // Constructor
    public NumberOperator() {}

    public long getWhichPrime(int n) {
        int primeCount = 0;
        long checkPrime = 1L;
        long foundPrime = 0L;
        while (primeCount < n) {
            if (isPrime(checkPrime)) {
                primeCount++;
                foundPrime = checkPrime;
            }
            checkPrime += 1L;
        }

        return foundPrime;
    }

    public boolean isPrime(long n) {
        if (n <= 3) {
            return (n > 1);
        } else if (n % 2 == 0 || n % 3 == 0) {
            return false;
        } else {
            for (int i = 5; i * i <= n; i += 6) {
                if ( n % i == 0 || n % (i + 2) == 0) {
                    return false;
                }
            }
            return true;
        }
    }
}
