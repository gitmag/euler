package euler;

/* What is the 10001st prime number */
class ProblemSeven {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Please enter the desired prime number");
        } else {
            int goToPrime = Integer.parseInt(args[0]);
            NumberOperator asdf = new NumberOperator();
            long desiredPrime = asdf.getWhichPrime(goToPrime);

            System.out.println("The " + Integer.toString(goToPrime) +
                " prime is " + Long.toString(desiredPrime));
        }
    }

}
