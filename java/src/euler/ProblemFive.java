/* Smallest positive number evenly divisible by all numbers from 1-20
 * 2520 is smallest from 1-10
 * Answer is 232792560 */

 class ProblemFive {
     public static void main(String[] args) {
         boolean haveAnswer = false;
         boolean notDivisible = false;
         long ans = 20L;
         while (!haveAnswer) {
             for (int x = 11;x < 21; x++) {
                 if (ans % x != 0) {
                     break;
                 }
                 if (x == 20) {
                     haveAnswer = true;
                     System.out.println(Long.toString(ans));
                 }
             }
             ans += 20L;
         }
     }
 }
