/** Answer is 4613732 */
class ProblemTwo {
	public static void main(String[] args) {
		int sum = 0;
		int prevNum = 0;
		int fibNum = 1;
		int curNum = 1;
		while (curNum < 4000000) {
			if (fibNum % 2 == 0) { sum += fibNum; }
			fibNum = curNum + prevNum;
			prevNum = curNum;
			curNum = fibNum;
		}
		FiboNum asdf = new FiboNum();
		int afd = asdf.recursion(7);
		System.out.println(afd);
		System.out.println(sum);
	}
}

class FiboNum {
	private int max = 4000000;
	
	public int recursion(int a) {
		if (a == 0) {
			return 0;
		}
		if (a == 1) {
			return 1;
		}
		return recursion(a - 1) + recursion(a - 2);
	}
}

