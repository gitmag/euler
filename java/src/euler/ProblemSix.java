/* Find the difference between sum of the squares, and square of sums
 * of the first 100 natural numbers
 * (1^2 + 2^2 ... 100^2) - (1 + 2 + ... + 100)^2
 * Answer is 25164150*/
class ProblemSix {
    public static void main(String[] args) {
        double sum = 0;
        double squareOfSums = 0;
        double sumOfSquares = 0;

        for (int i = 1; i < 101; i++) {
            sum += i;
            sumOfSquares += Math.pow(i, 2);
        }

        squareOfSums = Math.pow(sum, 2);
        long ans = (long)(sumOfSquares - squareOfSums);
        System.out.println(Long.toString(ans));
    }
}
