# find the sum of the even fibonacci numbers below 4.000.000
# 4613732

current = 1
previous = 0
total = 0
num = None

while current < 4000000:
	if (current % 2 == 0):
		total += current
	num = current + previous
	previous = current
	current = num

print total