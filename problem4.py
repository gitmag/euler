# Largest palindrome product of two 3-digit numbers
# ie 91 + 99 = 9009, lragest palindrome of two 2-digit numbers
# 906609

def palin():
	# RETURN!
	list = []
	for i in range(999,100,-1):
		for j in range(999, 100, -1):
			pal = str(i * j)

			if  pal == pal[::-1]:
				list.append(int(pal))
	return list

print(max(palin()))
