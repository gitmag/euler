# Find the difference between the sum of squares of first 100 natural numbers
# and the square of the sum (1^2 + 2^2 +...100^2) - (1+2...+10) ^ 2

sumofsquares = 0
squareofsums = 0

for x in range(1,101):
	sumofsquares += x**2

for x in range(1,101):
	squareofsums += x

print (sumofsquares - squareofsums**2)