# Smallest positive number divisible by all # from 1-20 without remainder
# can possibly check fewer numbers, ie if divisible by 4, divisible by 2
# should start with multiples of 20
# have to check 20, 19, 18, 17, 16, 15, 14, 13, 12, 11
start = 2520
jump = 20
nusu = True
while nusu:
	for x in range(11,21):
		if (start % x != 0):
			break
		if (x == 20):
			nusu = False
			print(start)
	start += jump
