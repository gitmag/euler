# The largest prime factor of 600851475143
# Start with 3, move up by 2, and decrease limit as we go
# Or find all primes up to somewhere less than half the target
import math

number = 600851475143
factor = []
i = 3
j = number
while (i < j):
	if number % i == 0:
		factor.append(i)
	j = number / i
	i = i + 2
print(factor)

for h in reversed(factor):
	if h % 2 == 0 and h > 2:
		continue
	if all(h % i for i in range(3, int(math.sqrt(h)) + 1, 2)):
		print(h)
